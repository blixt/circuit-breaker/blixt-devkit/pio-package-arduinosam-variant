#ifndef __BCB_H__
#define __BCB_H__

#include <cstdint>


typedef void (* BCBCallbackON)();
typedef void (* BCBCallbackZD)();
typedef void (* BCBCallbackOC)();
typedef void (* BCBCallbackOT)();

class BCB {
public:

  /**
   * @addtogroup BCB Main API
   * @{
   */
  /**
   * @brief Initialize the BCB.
   *        This function should be called prior to call any other function.
   */
  static void init();
  /**
   * @brief Turn on the BCB.
   */
  static void on();
  /**
   * @brief Turn off the BCB.
   */
  static void off();
  /**
   * @brief Turn on the LED on the BCB.
   */
  static void ledOn();
  /**
   * @brief Turn off the LED on the BCB.
   */
  static void ledOff();
  /**
   * @brief Get the status that if the BCB is on or off.
   *
   * @return true if the BCB is on. false if off.
   */
  static bool isOn();
  /**
   * @brief Get the status that if the internal sub-systems have power.
   *
   * @return true if the internal sub-systems have power. false otherwise.
   */
  static bool isPowerGood();
  /**
   * @brief Check if the over current protection is triggered.
   *
   * @return true if the over current protection is triggered.
   */
  static bool isOCPTriggered();
  /**
   * @brief Check if the over temperature protection of the transistors is triggered.
   *
   * @return true if the over temperature protection of the transistors is triggered.
   */
  static bool isOTPTriggered();
  /**
   * @brief Get the instantaneous current.
   *
   * @return Current in milliamperes
   */
  static int32_t getCurrent();
  /**
   * @brief Get the instantaneous voltage.
   *
   * @return Voltage in millivolts
   */
  static int32_t getVoltage();
  /**
   * @brief Set the Callback for the ON event.
   *
   * @param callback A pointer to the callback function
   */
  static void setCallbackON(BCBCallbackON callback);
  /**
   * @brief Set the Callback for the zero detection event.
   *
   * @param callback A pointer to the callback function
   */
  static void setCallbackZD(BCBCallbackZD callback);
  /**
   * @brief Set the Callback for the overcurrent event.
   *
   * @param callback A pointer to the callback function
   */
  static void setCallbackOC(BCBCallbackOC callback);
  /**
   * @brief Set the Callback for the over temperature event.
   *
   * @param callback A pointer to the callback function
   */
  static void setCallbackOT(BCBCallbackOT callback);
  /**
   * @}
   */

  /**
   * @addtogroup BCB Auxiliary API
   * @{
   */
  static uint32_t getADCCurrent1();
  static uint32_t getADCCurrent2();
  static uint32_t getADCVoltage();
  static uint32_t getADCTimestamp();

  static uint32_t getADCurrent1Zero();
  static uint32_t getADCurrent2Zero();
  static uint32_t getADCVoltageZero();

  static uint32_t getChipIDP1();
  static uint32_t getChipIDP2();
  static uint32_t getChipIDP3();
  static uint32_t getChipIDP4();

  static void reset();
  static void calibrate(bool vm, bool save);
  /**
   * @}
   */

protected:

  BCB();
  BCB(const BCB&);
  BCB& operator=(const BCB&);

  static void _onON();
  static void _onOCP();
  static void _onOTP();
  static void _onZDPulse();
  static void _adcInit();
  static void _dacInit();
  static uint32_t _readADC(uint8_t pin, uint8_t n_avg_samples_pw = 0);
  static void _writeDAC(uint32_t val);
  static void _enCMOPAmp(bool en);
  static void _enVMOPAmp(bool en);
  static void _setupTimestampTimer();
  static void _startSampling();
  static void _stopSampling();

  static void _flashGetInfo();
  static bool _flashEraseRow(uint32_t addr);
  static bool _flashWritePage(uint32_t addr, uint8_t* buf, uint32_t len);
  static bool _flashReadPage(uint32_t addr, uint8_t* buf, uint32_t len);

  friend void _adc_irq();

  static uint32_t _flash_page_size;
  static uint32_t _flash_n_pages;
  static uint32_t _flash_max_size;

  static volatile uint32_t _adc_c1;
  static volatile uint32_t _adc_c2;
  static volatile uint32_t _adc_v;
  static volatile uint32_t _adc_ts;
  static volatile uint32_t _ts_zd_prev;
  static uint32_t _adc_c1_zero;
  static uint32_t _adc_c2_zero;
  static uint32_t _adc_v_zero;
  static uint32_t _dac_c2_zero;

  static volatile bool _otp_triggered;
  static volatile bool _ocp_triggered;

  static BCBCallbackON _cb_on;
  static BCBCallbackZD _cb_zd;
  static BCBCallbackOC _cb_oc;
  static BCBCallbackOT _cb_ot;
};

#endif /* __BCB_H__ */
